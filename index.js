const VERSION = '1.0.0'

module.exports = {
    version : VERSION,
    parse: require('./src/parser')
}
