# Pseudo Code

### Comentarios

Para hacer comentarios de una linea utilizar la doble barra `//` 

Para comentarios multilinea iniciar con `/*` y finalizar con `*/`

### Variables

Para crear variables se utiliza la palabra reservada `variable`

```
variable nombre = 'John'
variable apellido = 'Doe'
variable nombreCompleto = nombre + apellido
```

### Constantes

Para crear constantes se utiliza la palabra reservada `constante`

```
constante HORAS_DIARIAS = 24
```

### Condicionales 

Las palabras `true` y `false` pueden reemplazarse por `verdadero` y `falso`

```
si es verdad que (verdadero) 
    imprimir "hola mundo!"
fin
```

### Operadores Matematicos

En lugar de simbolos matematicos se pueden usar las palabras equivalentes:
- sumar
- restar
- multiplicar por
- dividir por

```
variable add = 10 sumar 20
variable sub = 10 restar 20
variable mul = 10 multiplicar por 20
variable div = 10 dividir por 20
```

### Operadores de Comparacion

```
variable eq  = 10 es igual a 0
variable neq = 10 no es igual a 0
variable dis = 10 es distinto a 0
variable gt  = 10 es mayor que 0
variable gte = 10 es mayor o igual que 0
variable lt  = 10 es igual que 0
variable lte = 10 es menor o igual que 0
```

### Ciclos

Los ciclos inician con la palabra reservada `ciclo`
Las siguientes 3 lines deben contener:
1. la inicializacion de la variable
2. la comparacion de la variable
3. la modificacion de la variable
El bloque de codigo asociado al ciclo se encierra entre los delimitadores `comienzo` y `fin` 

```
ciclo
    variable contador = 1
    contador menor a 10
    contador++
comienzo
    imprimir contador
fin
```

### Estructuras

Para declarar una clase se utiliza la palabra reservada `clase` seguida de ':' y el nombre de la clase.
Si la clase no declara propiedades ni metodos no requiere nada a continuacion.

```
clase : Casa
```

#### Propiedades

Las propiedades se indican debajo del nombre `Propiedades` que debe escribirse tal cual.

```
clase : Casa
    Propiedades
    Metodos
```

Para declarar las propiedades se colocan a partir del bloque `Propiedades` en forma de `nombre : valor`

```
clase : Casa
    Propiedades
        habitantes
        enVenta : falso
        direccion : "Siempre Viva 123"
        pisos : 2
        espacios : ["living", "cocina", "garage"]
```

Para declarar los metodos se colocan a partir del bloque `Metodos` en forma de `nombre(a,b) {}`

Los metodos se escriben igual que las funciones comunes solo que sin necesidad de la palabra `funcion` antes del nombre.

El cuerpo del metodo se contiene dentro de los delimitadores `comienzo` y `fin`

Como buena practica se recomienda dejar una linea entre el fin de un metodo y el comienzo del proximo

Para hacer referencia a una propiedad de la clase dentro de los metodos se usa la palabra reservada `propiedad` seguida por un `.` y el nombre de la propiedad que se necesita invocar.

```
clase : Casa
    Propiedades
        habitantes
        enVenta : falso
        direccion : "Siempre Viva 123"
        pisos : 2
        espacios : ["living", "cocina", "garage"]
    Metodos
        setearHabitantes(numero)
            comienzo
                propiedad.habitantes = numero
            fin
        
        cantidadDeHabitantes()
            comienzo
                retornar propiedad.habitantes
            fin
```
