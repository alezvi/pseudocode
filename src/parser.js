const tokens = require('./tokens')
const loopParser = require('./loop')
const classParser = require('./classParser')

/**
 * @param {string} code
 * @returns {string}
 */
function parser(code) {
    const COMMENT_MULTILINE_REGEX = RegExp(/^\/\*[\s\S]*?\*\/$/, 'gm')
    const COMMENT_SINGLE_REGEX = RegExp(/\/\/.*\n?/, 'g')

    code = code.replace(COMMENT_MULTILINE_REGEX, '')
    code  = code.replace(COMMENT_SINGLE_REGEX, '')

    let lines = code.split('\n')

    let output = []

    lines.forEach(line => {
        /**
         * Cleanup blank spaces
         */
        line = line.trim()

        /**
         * If this line is a comment, will be ignored on compilation
         */
        if (line.startsWith('//')) return

        /**
         * Otherwise is code, will be used for output
         */
        output.push(line)
    })

    output = output.join('\n')

    /**
     * Pseudocode loop regex
     * @type {RegExp}
     */
    let loopRegex = /(ciclo.+comienzo)/gms

    /**
     * Check loops inside
     * @type {RegExpMatchArray}
     */
    let loop = output.match(loopRegex)

    /**
     * Replaced matched loops
     */
    if (loop) {
        let match = loop[0]
        let parsedLoop = loopParser(match)
        output = output.replace(match, parsedLoop)
    }

    /**
     * Replace other tokens
     */
    tokens.forEach(token => {
        if (output.includes(token.keyword)) {
            output = output.replace(token.regex, token.replace)
        }
    })

    let hasClass = output.match(/clase\s+\:/gm)

    if (hasClass) {
        try {
            output = classParser(output).toJS()
        } catch(err) {
            console.error(err)
        }
    }

    return output
}

module.exports = parser
