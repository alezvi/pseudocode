let comparison = require('./comparison')

module.exports = function(code) {
    let lines = code.split('\n')

    lines = lines.filter(line => line.trim().length > 0)

    let output = []

    let started = false

    let init = {
        variable: "",
        operator: "=",
        value: ""
    }

    let compare = {
        left: "",
        operator: "",
        right: "",
    }

    let progress = {
        variable: "",
        operator: "",
    }

    if (lines[0].includes('ciclo')) {
        started = true
    }

    if (lines[1].includes('=')) {
        let parts = lines[1].split('=')
        init.operator = '='
        init.variable = parts[0].replace('let', '').trim()
        init.value = parts[1].trim()
    }

    if (lines[2]) {
        let symbols = Object.keys(comparison)
        let keyword = symbols.find(s => lines[2].indexOf(s) > -1)

        if (comparison[keyword]) {
            compare.operator = comparison[keyword]
        } else {
            throw new Error(`Invalid operator "${keyword}"`)
        }

        let parts = lines[2].split(keyword)
        compare.left = parts[0].trim()
        compare.right = parts[1].trim()
    }

    if (lines[3]) {
        if (lines[3].includes('++')) {
            progress.variable = lines[3].replace('++', '').trim()
            progress.operator = '++'
        }

        if (lines[3].includes('--')) {
            progress.variable = lines[3].replace('--', '').trim()
            progress.operator = '--'
        }
    }

    return `
        for (
            ${init.variable} ${init.operator} ${init.value}; 
            ${compare.left} ${compare.operator} ${compare.right}; 
            ${progress.variable}${progress.operator}
        ) {
    `
}
