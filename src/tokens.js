module.exports = [
    {
        "keyword": "imprimir",
        "regex" : /(imprimir)\s+([^\n]+)/gm,
        "replace": "console.log($2)"
    },
    {
        "keyword": "variable",
        "regex" : /variable\s+([a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]+)(\s+\=[^\n]+)?/gm,
        "replace": "let $1 $2"
    },
    {
        "keyword": "constante",
        "regex" : /constante\s+([a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]+)(\s+\=[^\n]+)/gm,
        "replace": "const $1 $2"
    },
    {
        "keyword": "verdadero",
        "regex" : /verdadero/gm,
        "replace": "true"
    },
    {
        "keyword": "falso",
        "regex" : /falso/gm,
        "replace": "false"
    },

    /**
     * Delimiters
     */
    {
        "keyword": "si es verdad que",
        "regex" : /si\s+es\s+verdad\s+que\s+(\(.*\))\n/gm,
        "replace": "if $1 {\n\t"
    },
    {
        "keyword": "si no",
        "regex" : /si\s+no\s+/gm,
        "replace": "} else {\n"
    },
    {
        "keyword": "si",
        "regex" : /(si\s*)(\(.*\))\n/gm,
        "replace": "if $2 {\n\t"
    },
    {
        "keyword": "comienzo",
        "regex" : /comienzo/gm,
        "replace": " { "
    },
    {
        "keyword": "fin",
        "regex" : /fin/gm,
        "replace": " } "
    },
    {
        "keyword": "funcion",
        "regex" : /funcion\s+/gm,
        "replace": "function "
    },
    {
        "keyword": "fn",
        "regex" : /fn\s+/gm,
        "replace": "function "
    },
    {
        "keyword": "retornar",
        "regex" : /retornar\s+/gm,
        "replace": "return "
    },

    /**
     * Comparison
     */
    {
        "keyword": "no es igual",
        "regex" : /(.+)no\s+es\s+igual\s+[que|a]+(.+)/gm,
        "replace": " $1 != $2 "
    },
    {
        "keyword": "es igual a",
        "regex" : /\s+(es\s+igual\s+a)\s+/gm,
        "replace": " == "
    },
    {
        "keyword": "es igual",
        "regex" : /\s+(es\s+igual)\s+/gm,
        "replace": " == "
    },
    {
        "keyword": "es mayor",
        "regex" : /(.+)es\s+mayor\s+[que|a]+(.+)/gm,
        "replace": " $1 > $2 "
    },
    {
        "keyword": "es menor",
        "regex" : /(.+)es\s+menor\s+[que|a]+(.+)/gm,
        "replace": " $1 < $2 "
    },
    {
        "keyword": "es mayor o igual",
        "regex" : /(.+)es\s+mayor\s+\o\s+igual\s+[que|a]+(.+)/gm,
        "replace": " $1 >= $2 "
    },
    {
        "keyword": "es menor o igual",
        "regex" : /(.+)es\s+menor\s+\o\s+igual\s+[que|a]+(.+)/gm,
        "replace": " $1 <= $2 "
    },

    /**
     * Logic Operators
     */
    {
        "keyword": "y tambien",
        "regex" : / y tambien /gm,
        "replace": " && "
    },
    {
        "keyword": "y ademas",
        "regex" : / y ademas /gm,
        "replace": " && "
    },
    {
        "keyword": "o tambien",
        "regex" : / o tambien /gm,
        "replace": " || "
    },
    {
        "keyword": "o ademas",
        "regex" : / o ademas /gm,
        "replace": " || "
    },

    /**
     * Math symbols
     */
    {
        "keyword": "sumar",
        "regex" : / sumar /gm,
        "replace": " + "
    },
    {
        "keyword": "restar",
        "regex" : / restar /gm,
        "replace": " - "
    },
    {
        "keyword": "multiplicar por",
        "regex" : / multiplicar por /gm,
        "replace": " * "
    },
    {
        "keyword": "multiplicado por",
        "regex" : / multiplicado por /gm,
        "replace": " * "
    },
    {
        "keyword": "dividir por",
        "regex" : / dividir por /gm,
        "replace": " / "
    },
    {
        "keyword": "dividido por",
        "regex" : / dividido por /gm,
        "replace": " / "
    },

    {
        "keyword": ".total()",
        "regex" : /\.total\(\)/gm,
        "replace": ".length"
    },
    {
        "keyword": ".agregar(",
        "regex" : /\.agregar\s*\(/gm,
        "replace": ".push("
    },
    {
        "keyword": "nuevo",
        "regex" : /nuev[ao]\s+(\D\d)*/gm,
        "replace": "new $1"
    },
    {
        "keyword": "continuar",
        "regex" : /(\bcontinuar\b)/g,
        "replace": "continue"
    },
    {
        "keyword": "finalizar",
        "regex" : /(\bfinalizar\b)/g,
        "replace": "break"
    },
]
