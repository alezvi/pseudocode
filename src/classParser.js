/**
 * This module parses pseudocode for class
 *
 * @example
 *
 * class : Auto
 * Propiedades
 *  marca : "Fiad"
 *  kms : 0
 *  maxVelocity : 240
 * Metodos
 *  definirMarca(marca)
 *      comienzo
 *          propiedades.marca = marca
 *      fin
 *  obtenerMarca()
 *      comienzo
 *          retornar propiedades.marca
 *      fin
 * fin
 */

module.exports = function (code) {
    /**
     * @var {RegExp}
     */
    const CLASS_REGEX = /clase\s*\:[\s\S]+fin/gm

    let matched = code.match(CLASS_REGEX)

    if (matched) {
        code = matched[0]
    }

    code = code.replace(/comienzo/g, '{')
    code = code.replace(/fin/g, '}')

    let lines = code.split('\n')

    /**
     * Check if is property declaration
     *
     * @example
     * name : "John Doe"
     * age : 33
     * hobbies : ["music", "sports", "tv"]
     *
     * @var {RegExp}
     */
    const PROPERTY_DECLARATION = /^\s?\w+\s+\:\s+[^\n]+/gi

    /**
     * Check if is function signature
     */
    const FUNCTION_SIGNATURE = /^\s+?funcion\s+\w+\(\)/gi

    /**
     * The name of declared class
     * @var {string}
     */
    let className

    let propertiesBlockInitialized = false

    let methodsBlockInitialized = false

    let hasProperties = false

    let hasMethods = false

    let declaredProperties = {}

    let declaredMethods = []

    let finished = false

    let properties = []

    let inMethod = false

    let afterClass = ''

    lines = lines.filter(line => {
        return line.trim() !== ''
    })

    lines.forEach((line, index) => {
        if (index == 0) {
            className = line.split(':')[1].trim()
            return
        }

        if (line.trim().startsWith('Propiedades')) {
            propertiesBlockInitialized = true
            return
        }

        if (line.trim().startsWith('Metodos')) {
            methodsBlockInitialized = true
            return
        }

        if (propertiesBlockInitialized && !methodsBlockInitialized) {
            if (line.includes('}')) {
                propertiesBlockInitialized = false
                return
            }

            let parts = []

            if (line.includes(':')) {
                parts = line.split(':')
                parts[0] = parts[0].trim()
                parts[1] = parts[1].trim()
            } else {
                parts[0] = line.trim()
                parts[1] = null
            }

            properties.push(
                `this.${parts[0]} = ${parts[1]}`
            )

            return
        }

        if (methodsBlockInitialized) {
            let methodRegex = /^[a-zA-Z]*\s*\([^\)]*\)$/gm

            if (line.trim().match(methodRegex)) {
                declaredMethods.push(`${line.trim()}`)
                inMethod = true
                return
            }

            if (inMethod) {
                if (line.includes('}')) {
                    inMethod = false
                }

                if (line.includes('propiedad.')) {
                    line = line.replace(/propiedad\./g, 'this.')
                }

                if (line.includes('retornar ')) {
                    line = line.replace('retornar ', 'return ')
                }

                declaredMethods.push(line)
                return
            }
        }

        afterClass += '\n' + line
    })

    this.getClassName = function () {
        return className
    }

    this.getParsed = function () {
        return lines
    }

    this.hasProperties = function () {
        return propertiesBlockInitialized && hasProperties
    }

    this.hasMethods = function () {
        return methodsBlockInitialized && hasMethods
    }

    this.finished = function () {
        return finished
    }

    this.toJS = function () {
        declaredMethods.pop()
        
        return `
        class ${className} {
            constructor() {
                ${properties.join('\n')}
            }

            ${declaredMethods.join('\n')}
        }

        ${afterClass}
        `
    }

    return this
}
//
// let code = `
// clase : Auto
//     Propiedades
//         marca : "BMW"
//         color: "rojo"
//         kilometros: 100
//         pasajeros: [1,2,3,4]
//     Metodos
//         getMarca()
//             comienzo
//                 retornar propiedad.marca
//             fin
//
//         setMarca(marca)
//             comienzo
//                 propiedad.marca = marca
//             fin
// fin
// `
//
// let parser = parseClass(code)
//
// // console.log(parser.getParsed())
// // console.log('El nombre de la clase es ' + parser.getClassName())
// // console.log('La clase tiene propiedades ? ' + (parser.hasProperties() ? 'si' : 'no'))
// // console.log('La clase tiene metodos ? ' + (parser.hasMethods() ? 'si' : 'no'))
// // console.log('La clase ha sido finalizada ? ' + (parser.finished() ? 'si' : 'no'))
//
// console.log(parser.toJS())
